from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, DeleteForm, ItemForm


# Create your views here.
def todo_list_list(request):
    todos = TodoList.objects.all()
    context = {
        "todo_list_list": todos
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    list = get_object_or_404(TodoList, id=id)
    context = {
        "list_object": list,
    }
    return render(request, "todos/details.html", context)


def todo_list_create(request):
    if request.method == 'POST':
        form = TodoForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoForm()

    # put form in the context
    context = {
        "form": form,
    }
    # render the HTML template with the form
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    item = get_object_or_404(TodoList, id=id)
    if request.method == 'POST':
        # submitted the changes
        form = TodoForm(request.POST, instance=item)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        # requested the form
        form = TodoForm(instance=item)
    # put the form in context and render it
    context = {
        "form": form,
    }
    return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    item = get_object_or_404(TodoList, id=id)
    if request.method == 'POST':
        # delete the entry
        item.delete()
        return redirect('todo_list_list')
    else:
        # requested the form
        form = DeleteForm(instance=item)
    # put the form in context and render it
    context = {
        "form": form,
    }
    return render(request, "todos/delete.html", context)


def todo_item_create(request):
    if request.method == 'POST':
        form = ItemForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.list.id)
    else:
        form = ItemForm()

    # put form in the context
    context = {
        "form": form,
    }
    # render the HTML template with the form
    return render(request, "items/create.html", context)


def todo_item_update(request, id):
    # item = get_object_or_404(TodoItem, id=id)
    item = TodoItem.objects.get(id=id)
    if request.method == 'POST':
        # submitted the changes
        form = ItemForm(request.POST, instance=item)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", item.list.id)
    else:
        # requested the form
        form = ItemForm()
    # put the form in context and render it
    context = {
        "form": form,
        "item": item,
    }
    return render(request, "items/edit.html", context)
